import React from 'react';
import Cover from './images/cover.jpg';
import Aleja1 from './images/aleja1.jpg';
import Aleja2 from './images/aleja2.jpg';
import CasperNight from './images/casperNight.jpg';
import Mnl from './images/mnl.jpg';
import ValarMorgulis from './images/valarMorgulis.jpg';
import Amigosi from './images/amigosi.jpg';
import instagram from './images/footer/instagram.webp';
import phone from './images/footer/phone.webp';
import author from './images/footer/author.jpg';
import pitanje2 from './images/questions/pitanje2.jpg';
import pitanje3 from './images/questions/pitanje3.jpg';
import pitanje4 from './images/questions/pitanje4.jpg';

const Home = () => {
    return ( 
    <div >
      <img src={Cover} alt="" className="container-fluid" />
      <br/>
      <h2 className="title">Šta je to pab kviz?</h2>
      <p className="description">
        Pab kviz je nastao 70ih godina u Velikoj Britaniji sa ciljem da popularizuje pabove koji nisu bili poznati i za koje dosta ljudi nije čulo. 
        Pab kviz je mesto dobre zabave. Na ovom događaju ljudi u ekipama odgovaraju na pitanja iz različitih oblasti i na taj način se 
        takmiče sa ostalim ekipama. Postoji 7 oblasti na kvizu gde svaka ima 10 pitanja. Jedan tačan odgovor vredi jedan poen osim ako nije
        drugačije naglašeno (nekad nosi 1,5 - 2).  Kvizmaster čita jedno po jedno pitanje gde daje prostora otprilike 3-5 minuta po pitanju.
        Na kraju svake oblasti ekipe menjaju papirić sa ekipom pored i dok kvizmaster čita tačne odgovore ocenjuju i sabiraju međusobno.
        Nakon toga donose kvizmasteru, on unosi u tabelu i predstavljam im istu gde mogu da vide stanje. Bodovi se posle svake oblasti se 
        sabiraju i na kraju ekipa koja ima najviše osvaja kviz i glavnu nagradu. Postoji samo dva pravila:
        <h3>
          <b> 1. KVIZMASTER JE UVEK U PRAVU i  </b>
        </h3>
        <h3> 
          <b> 2. NEMA KORIŠĆENJA TELEFONA DOK TRAJE OBLAST. </b>
        </h3>
      </p>

      <div className="row">
            <div className="container">
                <img src={pitanje4} alt="" className="image" style={{width: '100%'}}/>
                <div className="middle">
                    <div className="text">Dan Mrtvih</div>
                </div>
            </div>
            <div className="container">
                <img src={pitanje2} alt="" className="image" style={{width: '100%'}}/>
                <div className="middle">
                    <div className="text">Fenerbahče i Galatasaraj</div>
                </div>
            </div>
            <div className="container">
                <img src={pitanje3} alt="" className="image" style={{width: '100%'}}/>
                <div className="middle">
                    <div className="text">Trokadero</div>
                </div>
            </div>
        </div>

      <p className="description">
        Pab kviz je osmišljen kao mesto na kom će se uživati u pabu (piću, hrani..) i usput rešavati zanimljiva pitanja. Nema negativnih 
        i glupih odgovora. Poenta je opuštanje gde je sve anonimno, odnosno samo za vašim stolom znate šta se dešava. Takođe, one 
        ekipe koje su najbolje dobijaju nagrade, a nešto slabije se trude da budu što bolje! Kviz vodi osoba koja se zove kvizmaster i
        koja određuje pravila kviza.
      </p>
      <div className="row">
            <div className="column">
                <img src={Aleja1} alt="" style={{width: '100%'}} className="zoom" />
            </div>
            <div className="column">
                <img src={Aleja2} alt="" style={{width: '100%'}} className="zoom"/>
            </div>
            <div className="column">
                <img src={CasperNight} alt="" style={{width: '100%'}} className="zoom" />
            </div>
        </div>

        {/* SA BOOTSTRAPOM */}


      {/* <Container>
        <Row>
          <Col>
            <Image src={Aleja1} alt=""  className="pictures-row"/>
          </Col>
          <Col>
            <Image src={Aleja2} alt="" className="pictures-row" />
          </Col>
          <Col>
            <Image src={CasperNight} alt="" className="pictures-row" />
          </Col>
        </Row>
      </Container> */}


      <h2 className="title">Koje oblasti postoje?</h2>
      <p className="description">
        Uglavnom se na pab kvizovima nalazi 7-8 oblasti (kod nas 8), a najčešće su: geografija, istorija, književnost (i umetnost) 
        sport, film (serija i TV), song quest (muzika u audio obliku, gde se pogađa ime pesme i ime izvođača), opšte znanje i 
        oblast večeri (svakog kviza je drugačija npr. Gospodar prstenova, automobilizam, Novi Sad, prepoznavanje sa slika itd.).
      </p>
      <div className="row">
            <div className="column">
                <img src={Amigosi} alt="" style={{width: '100%'}} className="zoom" />
            </div>
            <div className="column">
                <img src={Mnl} alt="" style={{width: '100%'}} className="zoom"/>
            </div>
            <div className="column">
                <img src={ValarMorgulis} alt="" style={{width: '100%'}} className="zoom"/>
            </div>
        </div>
      <h2 className="title">Kako prijaviti ekipu?</h2>
      <p className="description">
        Prijaviti se možete na više načina. Jedan od najlakših je na instagram stranici jednostavnom porukom. Možete se prijaviti i slanjem
        poruke na broj koji možete naći u kontaktima. 
        U poruci treba da napišete: 
        <h3>
          <b> 1. ime ekipe </b>
          <b>2. broj članova </b>
          <b>3. broj telefona. </b>
        </h3>
        Za sam kviz vam je potrebno samo dobro raspoloženje ( možda i malo znanja :) ).
      </p>
      <br/>

      <div className="row">
          <div className="column">
              <h3 className="center" style={{fontWeight: 'bold', fontSize: '40px'}}>
                <a href="https://www.instagram.com/kviz021/" style={{color: 'red'}}>@kviz021</a>
              </h3>
          </div>
          <div className="column">
            <h3 className="center" style={{fontWeight: 'bold', fontSize: '40px'}}>
              <a href="tel: +381-63-8298249" style={{color: 'red'}}>0638298249</a>
            </h3>
          </div>
          <div className="column">
            <h3 className="center" style={{fontWeight: 'bold', fontSize: '40px', color: 'red'}}>
              Autor i kvizmaster
            </h3>
          </div>
      </div>
      <div className="row">
          <div className="column">
              <a href="https://www.instagram.com/kviz021/">
              <img
               src={instagram}
               style={{width: '250px', height: '250px'}}
               className="center"
               alt=""
              />
            </a>
          </div>
          <div className="column" >
            <img 
              src={phone}
              style={{width: '250px', height: '250px'}}
              className="center"
              alt=""
            />
          </div>
          <div className="column" >
            <a href="https://www.instagram.com/tom5_kovic/">
              <img 
                src={author}
                style={{width: '250px', height: '240px', borderRadius: '50%'}}
                className="center"
                alt=""
              />
            </a>
          </div>
      </div>
      <br/>
      <br/>
    </div>
   );
  
};

export default Home;