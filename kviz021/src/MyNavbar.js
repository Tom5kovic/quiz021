import React from 'react';
import { Link } from 'react-router-dom';
import { Navbar, Nav } from "react-bootstrap";
import Logo from './images/logo.png';
import phone from './images/telephone.png';

const MyNavbar = () => {
  return (
    <Navbar bg="light" expand fixed="top">
        <Navbar.Brand>
            <Link to="/">
                <img src={Logo} className="Logo" />
            </Link>
        </Navbar.Brand>
        <Nav className="mr-auto">
            <Nav.Link as={Link} to="/quizmaster" className="link">
                Kviz-master
            </Nav.Link>
            <Nav.Link as={Link} to="/tournaments" className="link">
                Turniri
            </Nav.Link>
        </Nav>
        <div style={{marginRight: '20px'}}>
            <div>
                <img 
                    src={phone}  
                    alt=""
                    style={{height: '30px', width: '30px', marginRight: '15px'}}
                /> 
                <span style={{fontSize: '25px', color: 'dark'}}>
                    063 829 8249
                </span>
            </div>
            <div>
               <img 
                    src="https://www.enisa.europa.eu/topics/trainings-for-cybersecurity-specialists/online-training-material/images/whitakergroupgooglelocationicon.png/image" 
                    alt=""
                    style={{height: '30px', width: '20px', marginRight: '15px'}}
                />
                <span style={{fontSize: '25px', color: 'dark'}}>
                    <a href='https://www.instagram.com/crnaovcabrewery/'>
                        Crna ovca (Tehnolog's)
                    </a>
                </span> 
            </div>
        </div>
    </Navbar>
  );
};

export default MyNavbar;