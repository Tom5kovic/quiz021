import React from 'react';

const Footer = () => {
    return (
    <div className="footer">
        <br/>
        <h4 className="center" style={{fontSize: '20px'}}>© Kviz021, 2021.</h4>
    </div>
    )

};

export default Footer;