import React from 'react';
import author from './images/footer/author.jpg';

const Quizmaster = () => {
  return (
    <div>
      <div className="center" >
            <a href="https://www.instagram.com/tom5_kovic/">
              <img 
                src={author}
                style={{width: '250px', height: '240px', borderRadius: '50%'}}
                className="center"
                alt=""
              />
            </a>
          </div>
      <p className="description">
        Osnivač, autor svih pitanja i voditelj kviza, Tomislav Petković rodom iz Bukovca (i njime se ponosi) kraj Novog Sada.
        Pozitivac koji voli ljudi i njihovo okruženje. Često gleda evropske filmove koji mu uopše nisu zabavni.
        Voli pivo  više nego samog sebe, dobru klopu, putovanja, zabavu i avanturu. Diplomirani turizmolog u svetu veb developmenta. 
        Ljubitelj svih sportova iako se ni u jednom ne snalazi. Kaže da voli adrenalin ali se i većeg toboga plaši. 
        Voli da uživa u životu, često na roštilju sa društvom uz gajbu piva.
        Svima se hvali da je bio u SAD-u. Do sada ni na jednom TV kvizu ali biće kad nauči nešto. Jedini licencirani spasilac koji
        ne može da preroni olimpijski bazen po širini.  Slabo čita ali zato skoro nikada ne gleda TV. 
        Jednom prilikom je zatvorenih očiju pogodio centar u pikadu.
      </p>
    </div>
  );
};

export default Quizmaster;