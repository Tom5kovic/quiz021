import React from 'react';
import detelina from './images/tournaments/1/detelina.jpg';
import sebri from './images/tournaments/1/sebri.jpg';
import kupusi from './images/tournaments/1/kupusi.jpg';
import brljavci2 from './images/tournaments/2/brljavci.jpg';
import kupusi2 from './images/tournaments/2/kupusi.jpg';
import kventi2 from './images/tournaments/2/kventi.jpg';
import delta3 from './images/tournaments/3/delta.jpg';
import miro3 from './images/tournaments/3/miro.jpg';
import pjer3 from './images/tournaments/3/pjer.jpg';
import detelina4 from './images/tournaments/4/detelina.jpg';
import kupusi4 from './images/tournaments/4/kupusi.jpg';
import mnl4 from './images/tournaments/4/mnl.jpg';
import mnl5 from './images/tournaments/5/mnl.jpg';
import nadmetaci5 from './images/tournaments/5/nadmetaci.jpg';
import sebri5 from './images/tournaments/5/sebri.jpg';
import kupusi6 from './images/tournaments/6/kupusi.jpg';
import nadmetaci6 from './images/tournaments/6/nadmetaci.jpg';
import paori6 from './images/tournaments/6/paori.jpg';


const Tournaments = () => {
  return (
    <div>
      <h1 className="title">O turniru</h1>
        <p className="description">
            Do sada smo održali 6 turnira. Svaki turnir se sastojao od dva polufinala i finala, gde su prve 4 ekipe iz polufinala
            direktno prolazile da se takmiče za pehar. Najuspešnija je ekipa 'Mnogo neprijatni likovi' dok su poslednji turnir osvojili 'Nadmetači'.
        </p>

        <h2 className="title">TURNIR #1</h2>

        <div className="row">
            <div className="container">
                <img src={sebri} alt="" className="image" style={{width: '100%'}}/>
                <div className="middle">
                    <div className="text"> 1. mesto: Sebri </div>
                </div>
            </div>
            <div className="container">
                <img src={detelina} alt="" className="image" style={{width: '100%'}}/>
                <div className="middle">
                    <div className="text"> 2. mesto: Detelina sa 4 lista </div>
                </div>
            </div>
            <div className="container">
                <img src={kupusi} alt="" className="image" style={{width: '100%'}}/>
                <div className="middle">
                    <div className="text"> 3. mesto: Q-pussy </div>
                </div>
            </div>
        </div>
        <h2 className="title">TURNIR #2</h2>
        <div className="row">
            <div className="container">
                <img src={kupusi2} alt="" className="image" style={{width: '100%'}}/>
                <div className="middle">
                    <div className="text"> 1. mesto: Q-pussy </div>
                </div>
            </div>
            <div className="container">
                <img src={kventi2} alt="" className="image" style={{width: '100%'}}/>
                <div className="middle">
                    <div className="text"> 2. mesto: Tventi Karantino</div>
                </div>
            </div>
            <div className="container">
                <img src={brljavci2} alt="" className="image" style={{width: '100%'}}/>
                <div className="middle">
                    <div className="text"> 3. mesto: Brljavci</div>
                </div>
            </div>
        </div>
        <h2 className="title">TURNIR #3</h2>
        <div className="row">
            <div className="container">
                <img src={miro3} alt="" className="image" style={{width: '100%'}}/>
                <div className="middle">
                    <div className="text"> 1. mesto: Miro Škoro </div>
                </div>
            </div>
            <div className="container">
                <img src={pjer3} alt="" className="image" style={{width: '100%'}}/>
                <div className="middle">
                    <div className="text"> 2. mesto: Pjer Đoković</div>
                </div>
            </div>
            <div className="container">
                <img src={delta3} alt="" className="image" style={{width: '100%'}}/>
                <div className="middle">
                    <div className="text"> 3. mesto: Delta</div>
                </div>
            </div>
        </div>
        <h2 className="title">TURNIR #4</h2>
        <div className="row">
            <div className="container">
                <img src={mnl4} alt="" className="image" style={{width: '100%'}}/>
                <div className="middle">
                    <div className="text"> 1. mesto: Mnogo neprijatni likovi </div>
                </div>
            </div>
            <div className="container">
                <img src={kupusi4} alt="" className="image" style={{width: '100%'}}/>
                <div className="middle">
                    <div className="text"> 2. mesto: Q-pussy</div>
                </div>
            </div>
            <div className="container">
                <img src={detelina4} alt="" className="image" style={{width: '100%'}}/>
                <div className="middle">
                    <div className="text"> 3. mesto: Detelina sa 4 lista</div>
                </div>
            </div>
        </div>
        <h2 className="title">TURNIR #5</h2>
        <div className="row">
            <div className="container">
                <img src={mnl5} alt="" className="image" style={{width: '100%'}}/>
                <div className="middle">
                    <div className="text"> 1. mesto: Mnogo neprijatni likovi </div>
                </div>
            </div>
            <div className="container">
                <img src={sebri5} alt="" className="image" style={{width: '100%'}}/>
                <div className="middle">
                    <div className="text"> 2. mesto: Sebri</div>
                </div>
            </div>
            <div className="container">
                <img src={nadmetaci5} alt="" className="image" style={{width: '100%'}}/>
                <div className="middle">
                    <div className="text"> 3. mesto: Nadmetači</div>
                </div>
            </div>
        </div>
        <h2 className="title">TURNIR #6</h2>
        <div className="row">
            <div className="container">
                <img src={nadmetaci6} alt="" className="image" style={{width: '100%'}}/>
                <div className="middle">
                    <div className="text"> 1. mesto: Nadmetači </div>
                </div>
            </div>
            <div className="container">
                <img src={kupusi6} alt="" className="image" style={{width: '100%'}}/>
                <div className="middle">
                    <div className="text"> 2. mesto: Q-pussy</div>
                </div>
            </div>
            <div className="container">
                <img src={paori6} alt="" className="image" style={{width: '100%'}}/>
                <div className="middle">
                    <div className="text"> 3. mesto: Paori</div>
                </div>
            </div>
        </div>
    </div>
  );
};

export default Tournaments;