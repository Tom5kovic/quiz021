import React from 'react';
import { BrowserRouter as Router, Route ,Switch} from 'react-router-dom';
import MyNavbar from './MyNavbar';
import Home from './Home';
import Quizmaster from './Quizmaster';
import Error from './Error';
import Tournaments from './Tournaments';
import Footer from './Footer';

function App() {
    return (
        <div>
            <Router>
               <MyNavbar/> 
               <br/>
               <br/>
               <br/>
               <br/>
                <Switch>
                    <Route exact path="/">
                        <Home/> 
                    </Route>
                    <Route exact path="/quizmaster">
                        <Quizmaster/> 
                    </Route>
                    <Route exact path="/tournaments">
                        <Tournaments/> 
                    </Route>
                    <Route path='*'>
                        <Error />
                    </Route>
                </Switch>
            </Router>
            <Footer/>
        </div>
    )
}

export default App
